package com.waylau.spring.cloud.weather.job;

import com.waylau.spring.cloud.weather.service.CityDataService;
import com.waylau.spring.cloud.weather.service.WeatherDataService;
import com.waylau.spring.cloud.weather.vo.City;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author 贾文静 on 2018/5/30.
 * Describe
 */
@Slf4j
public class WeatherDataSyncJob extends QuartzJobBean {

    @Autowired
    private CityDataService cityDataService;
    @Autowired
    private WeatherDataService weatherDataService;

    /**
     * @param context
     * @see #execute
     */
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("Weather Data sync Job.start");
//        1、获取城市Id列表
        List <City> cityList = null;
        try {
            cityList = cityDataService.listCity();
        } catch (Exception e) {
            log.error("Exception！", e);
        }
        //2、遍历城市Id获取天气
        if (!CollectionUtils.isEmpty(cityList)) {
            cityList.forEach(city -> {
                weatherDataService.syncDateByCityId(city.getCityId());
                log.info("Weather Data sync Job CityId :"+city.getCityName());
            });
        }
        log.info("Weather Data sync Job.end");
    }
}
