package com.waylau.spring.cloud.weather.service.impl;

import com.waylau.spring.cloud.weather.service.CityDataService;
import com.waylau.spring.cloud.weather.until.XmlBuilder;
import com.waylau.spring.cloud.weather.vo.City;
import com.waylau.spring.cloud.weather.vo.CityList;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author 贾文静 on 2018/5/30.
 * Describe
 */
@Service
public class CityDataServiceImpl implements CityDataService {
    /**
     * 获取City列表
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<City> listCity(){
        //1、读取xml文件
        CityList cityList = null;
        try {
            Resource resource = new ClassPathResource("citylist.xml");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream(), "utf-8"));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
            bufferedReader.close();
            //2、将xml转为Java对象
            cityList = (CityList) XmlBuilder.xmlStrToObject(CityList.class, buffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityList.getCityList();
    }
}
