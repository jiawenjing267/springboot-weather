package com.waylau.spring.cloud.weather.until;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.Reader;
import java.io.StringReader;

/**
 * @author 贾文静 on 2018/5/30.
 * Describe
 */
public class XmlBuilder {
    /**
     * 将xml转换指定POJO对象
     * @param clazz
     * @param xmlStr
     * @return
     * @throws Exception
     */
    public static Object xmlStrToObject(Class<?> clazz,String xmlStr) throws Exception {
        JAXBContext context = JAXBContext.newInstance(clazz);

        //xml转为对象的接口
        Unmarshaller unmarshaller = context.createUnmarshaller();

        Reader reader = new StringReader(xmlStr);
        Object xmlObject =unmarshaller.unmarshal(reader);
        if (reader != null) {
            reader.close();
        }
        return xmlObject;
    }
}
