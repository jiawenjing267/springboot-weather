package com.waylau.spring.cloud.weather.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 贾文静 on 2018/5/24.
 * Describe
 */
@RestController
public class HelloController {

//    @RequestMapping("/hello")
    @GetMapping("/hello")
    public String hello() {
        return "hello world";
    }
}
