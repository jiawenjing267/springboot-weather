package com.waylau.spring.cloud.weather.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waylau.spring.cloud.weather.service.WeatherDataService;
import com.waylau.spring.cloud.weather.vo.WeatherResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe
 */
@Slf4j
@Service("weatherDataService")
public class WeatherDataServiceImpl implements WeatherDataService {
    private static final String WEATHER_URI = "http://wthrcdn.etouch.cn/weather_mini?";

    private static final long TIME_OUT = 1800L;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //region getDataByCityId+根据城市Id查询天气数据

    /**
     * 根据城市Id查询天气数据
     *
     * @param cityId 城市id
     * @return
     */
    @Override
    public WeatherResponse getDataByCityId(String cityId) {
        String uri = WEATHER_URI + "citykey=" + cityId;
        return this.doGetWeather(uri);
    }
    //endregion

    //region getDataByCityName+根据城市名称查询天气数据

    /**
     * 根据城市名称查询天气数据
     *
     * @param cityName 城市名称
     * @return
     */
    @Override
    public WeatherResponse getDataByCityName(String cityName) {
        String uri = WEATHER_URI + "city=" + cityName;
        return this.doGetWeather(uri);
    }
    //endregion

    //region private doGetWeather 通过httpclient访问天气数据并放入缓存

    /**
     * 通过httpclient访问天气数据并放入缓存
     *
     * @param uri 路径地址
     * @return
     */
    private WeatherResponse doGetWeather(String uri) {
        String strBody = null;
        ObjectMapper mapper = new ObjectMapper();
        WeatherResponse weather = null;
        ValueOperations <String, String> ops = stringRedisTemplate.opsForValue();
        //先查缓存，缓存有的取缓存中的数据
        if (stringRedisTemplate.hasKey(uri)) {
            strBody = ops.get(uri);
        } else {
            // 缓存中没有，再调用服务
            ResponseEntity <String> respString = restTemplate.getForEntity(uri, String.class);
            if (respString.getStatusCodeValue() == 200) {
                strBody = respString.getBody();
            }
            //写入到缓存中
            ops.set(uri, strBody, TIME_OUT, TimeUnit.SECONDS);
        }
        try {
            weather = mapper.readValue(strBody, WeatherResponse.class);
        } catch (IOException e) {
            log.error("Error", e);
        }
        return weather;
    }
    //endregion

    /**
     * 根据城市Id来同步天气
     * @param cityId 城市id
     */
    @Override
    public void syncDateByCityId(String cityId) {
        String uri = WEATHER_URI + "citykey=" + cityId;
        this.saveWeatherData(uri);
    }

    //region private saveWeatherData+保存天气数据到缓存中
    /**
     * 保存天气数据到缓存中
     *
     * @param uri
     */
    private void saveWeatherData(String uri) {
        String strBody = null;
        ValueOperations <String, String> ops = stringRedisTemplate.opsForValue();
        ResponseEntity <String> respString = restTemplate.getForEntity(uri, String.class);
        if (respString.getStatusCodeValue() == 200) {
            strBody = respString.getBody();
        }
        //写入到缓存中
        ops.set(uri, strBody, TIME_OUT, TimeUnit.SECONDS);
    }
    //endregion
}
