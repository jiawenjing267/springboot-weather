package com.waylau.spring.cloud.weather.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe 未来天气描述
 */
@Data
public class Forecast implements Serializable {
    private static final long serialVersionUID = 7947684575905942417L;
    private String date;
    private String high;
    private String fengli;
    private String low;
    private String fengxiang;
    private String type;

}
