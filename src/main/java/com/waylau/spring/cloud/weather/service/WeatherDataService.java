package com.waylau.spring.cloud.weather.service;

import com.waylau.spring.cloud.weather.vo.WeatherResponse;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe
 */
public interface WeatherDataService {

    /**
     * 根据城市Id查询天气数据
     * @param cityId 城市id
     * @return
     */
    WeatherResponse getDataByCityId(String cityId);

    /**
     * 根据城市名称查询天气数据
     *
     * @param cityName 城市名称
     * @return
     */
    WeatherResponse getDataByCityName(String cityName);

    /**
     * 根据城市Id来同步天气
     * @param cityId 城市id
     */
    void syncDateByCityId(String cityId);

}
