package com.waylau.spring.cloud.weather.service;

import com.waylau.spring.cloud.weather.vo.Weather;

/**
 * @author 贾文静 on 2018/5/30.
 * Describe
 */
public interface WeatherReportService {

    /**
     * 根据城市id查询天气预报接口
     * @param cityId 城市id
     * @return 天气情况
     */
    Weather getDataByCityId(String cityId);
}
