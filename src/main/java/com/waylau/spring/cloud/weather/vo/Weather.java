package com.waylau.spring.cloud.weather.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe 天气信息
 */
@Data
public class Weather implements Serializable {

    private static final long serialVersionUID = -4919087752294646097L;
    private String city;
    private String aqi;
    private String ganmao;
    private String wendu;
    private Yesterday yesterday;
    private List <Forecast> forecast;

}
