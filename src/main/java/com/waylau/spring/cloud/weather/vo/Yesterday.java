package com.waylau.spring.cloud.weather.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe
 */
@Data
public class Yesterday implements Serializable {
    private static final long serialVersionUID = 1190479434215109690L;
    private String date;
    private String high;
    private String fx;
    private String low;
    private String fl;
    private String type;

}
