package com.waylau.spring.cloud.weather.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 贾文静 on 2018/5/26.
 * Describe 数据天气的响应接口
 */

@Data
public class WeatherResponse implements Serializable {
    private static final long serialVersionUID = 9131849197706157466L;
    private Weather data;
    private Integer status;
    private String desc;

}
