package com.waylau.spring.cloud.weather.service;

import com.waylau.spring.cloud.weather.vo.City;

import java.util.List;

/**
 * @author 贾文静 on 2018/5/30.
 * Describe
 */
public interface CityDataService {


    /**
     * 获取City列表
     * @return
     * @throws Exception
     */
    List <City> listCity();
}
